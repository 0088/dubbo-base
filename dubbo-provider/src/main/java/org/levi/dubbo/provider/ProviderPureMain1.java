package org.levi.dubbo.provider;


import org.apache.dubbo.config.ProviderConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;

public class ProviderPureMain1 {

    public static void main(String[] args) throws IOException {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyProviderConfig.class);
        context.start();
        System.in.read();
    }

    @PropertySource("classpath:/dubbo-provider1.properties")
    @Configuration
    @EnableDubbo(scanBasePackages="org.levi.dubbo.provider.service")
    static class MyProviderConfig{

        @Bean
        public RegistryConfig registryConfig(){
            RegistryConfig registryConfig = new RegistryConfig();
            registryConfig.setAddress("zookeeper://127.0.0.1:2181?timeout=10000");
            return registryConfig;
        }

        @Bean
        public ProviderConfig providerConfig(){
            ProviderConfig providerConfig = new ProviderConfig();
            providerConfig.setThreadpool("myPool");
            providerConfig.setContextpath("/helloService");
            return providerConfig;
        }
    }
}
