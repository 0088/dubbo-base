package org.levi.demo.router;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.cluster.Router;
import org.apache.dubbo.rpc.cluster.RouterFactory;
@Activate
public class MyDubboRouterFactory implements RouterFactory {
    public Router getRouter(URL url) {
        return new MyDubboRouter(url);
    }
}
