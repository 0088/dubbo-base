package org.levi.dubbo.service;

import org.springframework.stereotype.Component;

@Component
public class HelloServiceMock implements HelloService {
    public String sayHello(String name) {
        return "hello " + name + " from mock";
    }
}
