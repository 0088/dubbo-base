package org.levi.dubbo;

import org.apache.dubbo.config.ConsumerConfig;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.levi.dubbo.test.ConsumerComponent;
import org.springframework.context.annotation.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ConsumerPureMain {

    public static void main(String[] args) throws IOException, InterruptedException {

        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        // 获取消费者组件
        ConsumerComponent service = context.getBean(ConsumerComponent.class);
       /* ConsumerComponent service2 = context.getBean(ConsumerComponent.class);
        System.out.println(service == service2);*/
        while(true){
            System.in.read();
            service.sayHello("dubbo");
        }

        /*for (int i = 0; i < 20000; i++) {
            TimeUnit.MILLISECONDS.sleep(5);
            new Thread(new Runnable() {
                public void run() {

                    service.sayHello("dubbo");
                }
            }).start();
        }*/
    }

    @Configuration
    @EnableDubbo
    @ComponentScan("org.levi.dubbo")
    @PropertySource("classpath:/dubbo-consumer.properties")
    static class ConsumerConfiguration {

        @Bean
        public ConsumerConfig consumerConfig() {
            ConsumerConfig consumerConfig = new ConsumerConfig();
//            consumerConfig.setTimeout(4000);
            return consumerConfig;
        }
    }

}
