package org.levi.dubbo.provider.service;

import org.apache.dubbo.config.annotation.Service;
import org.levi.dubbo.service.HelloService;

import java.util.concurrent.TimeUnit;

@Service(path = "/helloService")
public class HelloServiceImpl implements HelloService {

    public String sayHello(String name) {

        try {
            TimeUnit.MILLISECONDS.sleep(1000);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
        return "hello" + name;
    }
}


