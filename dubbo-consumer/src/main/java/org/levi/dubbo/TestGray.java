package org.levi.dubbo;

import org.levi.demo.router.HostRouterManager;

public class TestGray {

    public static void main(String[] args) throws Exception {

        HostRouterManager instance = HostRouterManager.getInstance();
        //instance.removeServiceGrayStatus("192.168.23.1","dubbo-provider");
        instance.addServiceToGray("192.168.23.1","dubbo-provider");
    }
}
