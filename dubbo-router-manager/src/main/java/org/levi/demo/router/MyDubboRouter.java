package org.levi.demo.router;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.RpcException;
import org.apache.dubbo.rpc.cluster.Router;

import java.util.List;
import java.util.stream.Collectors;

public class MyDubboRouter implements Router {

    private URL url;

    private final HostRouterManager hostRouterManager = HostRouterManager.getInstance();

    public MyDubboRouter(URL url) {
        this.url = url;
    }

    public URL getUrl() {
        return this.url;
    }

    public <T> List<Invoker<T>> route(List<Invoker<T>> invokers, URL url, Invocation invocation) throws RpcException {

        return invokers.stream().filter(tInvoker -> !hostRouterManager.isCurrentGray(tInvoker.getUrl().getIp(),tInvoker.getUrl().getParameter("remote.application"))).collect(Collectors.toList());
    }

    public boolean isRuntime() {
        return false;
    }

    public boolean isForce() {
        return false;
    }

    public int getPriority() {
        return 0;
    }
}
