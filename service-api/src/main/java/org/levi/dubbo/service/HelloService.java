package org.levi.dubbo.service;

/**
 *
 */
public interface HelloService {

    /**
     * @param name
     * @return
     */
    String sayHello(String name);
}
