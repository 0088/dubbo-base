package org.levi.dubbo.test;

import org.apache.dubbo.config.annotation.Reference;
import org.levi.dubbo.service.HelloService;
import org.springframework.stereotype.Component;

@Component
public class ConsumerComponent {

    @Reference(mock= "true",check = false)
    private HelloService helloService;

    public void sayHello(String name) {
        System.out.println(helloService.sayHello(name));
    }
}
