package org.levi.demo.router;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

public class MyZkClient {

    private final CuratorFramework zkClient;
    private volatile static MyZkClient myZkClient = null;

    private MyZkClient(CuratorFramework zkClient) {
        this.zkClient = zkClient;
    }

    public static MyZkClient getInstance(){
        if(myZkClient == null){
            synchronized (MyZkClient.class){
                if(myZkClient == null){
                    //创建zookeeper连接
                    RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000,3);
                    CuratorFramework curatorFramework = CuratorFrameworkFactory.newClient("127.0.0.1:2181",retryPolicy);
                    myZkClient = new MyZkClient(curatorFramework);
                    curatorFramework.start();
                }
            }
        }
        return myZkClient;
    }

    public CuratorFramework getZkClient(){
        return this.zkClient;
    }
}
